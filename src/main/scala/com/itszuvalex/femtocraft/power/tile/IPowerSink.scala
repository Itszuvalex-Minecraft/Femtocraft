package com.itszuvalex.femtocraft.power.tile

import com.itszuvalex.femtocraft.logistics.distributed.ITaskProvider

/**
  * Created by Christopher Harris (Itszuvalex) on 1/30/2016.
  */
trait IPowerSink extends ITaskProvider with ITilePower

